class MyStupidClass():
    instances = []
    def __init__(self, name):
        self.name = name
        MyStupidClass.instances.append(self)

    def print_names():
        for inst in MyStupidClass.instances:
            print(inst.name)

def main():
    inst01 = MyStupidClass("grust'")
    inst02 = MyStupidClass("pechal'")
    inst02 = MyStupidClass("toska")

    MyStupidClass.print_names()

if __name__ == '__main__':
    main()
