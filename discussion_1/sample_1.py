# -*- coding: utf-8 -*-

# from array import array

def some_parsing_result():
	return u'Тут какой-то текст, он всех печалит'

def main():
	russian_string = some_parsing_result() 
	# print(russian_string)

	print("\N{GREEK CAPITAL LETTER DELTA}")
	print("\u0394")

	print("-------------------------------------------")

	lol = '' #encoding='ascii' приведёт к ошибке
	with open("text.txt", 'r', encoding='utf-8') as f:
		lol = f.readline()
		# lol = '\n'.join(f.readlines())

	print(lol + '\n' + russian_string)

	print("-------------------------------------------")

	print('хрень') 
	print(u'хрень')
	print(u'хрень'.encode('utf-8'))

	print("-------------------------------------------")

if __name__ == "__main__":
	main()
