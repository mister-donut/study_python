# -*- coding: utf-8 -*-

def fibonacci(n):
	if n == 0:
		return 0
	if n == 1:
		return 1

	return fibonacci(n - 1) + fibonacci(n - 2)


def factorial_recursive(n):
	print("Вызван factorial_recursive({})".format(n))
	if n <= 1:
		return 1
	else:
		return n * factorial_recursive(n - 1)


def factorial(n):
	result = 1
	for i in range(1, n + 1):
		result = result * i

	return result


def main():
	print(factorial_recursive(10))
	print(factorial(10))


if __name__ == "__main__":
	main()
