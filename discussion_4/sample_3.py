# -*- coding: utf-8 -*-

def my_first_closure(number):
	def printer():
		"Here we are using the nonlocal keyword"
		nonlocal number
		# print(number)
		number=3
		print(number)
	printer()
	print(number)


def my_first_generator():
	some_data = range(10)
	for i in some_data:
		yield i**2

def main():
	# for i in my_first_generator:
	# 	print(i)

	my_first_closure(123)

if __name__ == "__main__":
	main()
