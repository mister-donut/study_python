# -*- coding: utf-8 -*-

import os
import xml.dom.minidom

def main():
	XML_FILE = os.path.join(os.getcwd(), 'sample2.xml')

	try:
		doc = xml.dom.minidom.parse(XML_FILE)
		node = doc.documentElement

		if node.nodeType == xml.dom.Node.ELEMENT_NODE:
			print('Элемент: %s' % node.nodeName)
			for (name, value) in node.attributes.items():
				print(' Attr -- имя: %s значение: %s' % (name, value))
		
		for appointment in node.getElementsByTagName("appointment"):
			subj = appointment.getElementsByTagName("subject")[0]

			txt = subj.childNodes[0]
			if txt.nodeType == txt.TEXT_NODE:
				print(txt.data)

	except IOError as e:
		print('nERROR - cant find file: %sn' % e)

if __name__ == "__main__": 
	main()
