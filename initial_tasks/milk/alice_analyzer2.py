removefromtext = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		  ' II.',' III.',' IV.',' V.',' VI.',' VII.',' VIII.',' IX.',' X.',' XI.',' XII.',
		  " ' ", "\n'","''", "'\n", '\n', " '", "' ",
		  ".'", ",'", "?'", "!'",
		  ';', ':', '--', '(', ')', '[', ']',
                  '. ', '*', ', ',
                  '.', '! ', '? ', '_',
                  '!', '?', ',', '"'] 
wordlist = []
reduced_wordlist = []
wordcounter = [0]
index = 0
alice_word_numbers_list = []
alice_sorted_list = []
alice_sorted_words = []

with open("Alice.txt") as textfile:
    text = textfile.read()
    #убрать возможные знаки препиинания и цифры - римские тоже(так как задание считать именно слова)
    #(тут конечно не предусмотреть все возможные варианты, но такой вариант плюс минус работает)
    for symbol in removefromtext:
            text = text.replace(symbol," ") 

    #убрать оставшиеся одинарные кавычки в начале и конце слов - из-за одного неприятного места в тексте, где подряд идут '' и ""
    for symbol in removefromtext[26:28]:
            text = text.replace(symbol,"") 


    #разбить весь текст на список по признаку, что между словами пробелы
    text = text.split(' ')
    for word in text:

        #Всё кроме 'I' делаем нижним регистром, чтобы одни и те же слова не считать за разные.
        #Это, конечно, знатная недоработка(( так как по-хорошему нужно различать имена собственные и нет.
        #Но я посчитала для простоты - пусть лучше я потеряю информацию об именах собственных, чем The и the, например,
        #буду считать разными словами,
        #а еще исключаем из списка пробелы, так как они после replace остались в некоторых местах
        if word != "I" and word != '' :
            word = word.lower() 
            wordlist.append(word)
        elif word == "I":
            wordlist.append(word)


reduced_wordlist.append(wordlist[0])

for w in wordlist:
        if w in reduced_wordlist:
                index = reduced_wordlist.index(w)
                wordcounter[index] = wordcounter[index]+1
                
        else:
                reduced_wordlist.append(w)
                wordcounter.append(1)

#объединение списков в 1
if len(reduced_wordlist) == len(wordcounter): #равную длину считаем условием, что количество посчитано правильно
        alice_word_numbers_list = list(zip(reduced_wordlist, wordcounter))

#запись объединенного
outfile = open("alice_word_numbers_list.txt", "w")
for k in alice_word_numbers_list:
    outfile.write('{} - {} \n'.format(*k))

##################################################################################################################

#ключ для сортировки по величине 2го элемента
def sort_key(a):
    return a[1]

#сортировка по величине
alice_sorted_list = (sorted(alice_word_numbers_list,key=sort_key))

#убрать величины и оставить только слова
alice_sorted_words = [element[0] for element in alice_sorted_list]

#ввод числа - сколько самых часто встречающихся слов записать в вфайл?
print('Введите, сколько слов писать в файл')
N = int(input())

#запись нужнгого числа самых часто встречающихся слов в файл
#так как отсортировано по возрастанию, а не по убыванию - идем от последнего индекса с шагом -1
outfile2 = open("alice_list_N_most_freq.txt", "w")
last_index = len(alice_sorted_list)
first_index = len(alice_sorted_list)-(int(N)+1)

#обрезать то, что не вошло в требуемый диапазон
words_sorted_by_freq = alice_sorted_words[last_index : first_index : -1]

#запись в файл
for word in words_sorted_by_freq:
    outfile2.write(str(word) + '\n')

textfile.close()
outfile.close()
outfile2.close()
